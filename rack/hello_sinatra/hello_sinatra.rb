require 'sinatra'

post '/hello' do
  'Post method!!!'
end

delete '/hello' do
  'Hello!'
end

get '/hello' do
  'Hello, world!'
end

get /^\/hello\/([\d]+)$/ do |n|
  "Numeric value: #{n}"
end

get '/hello/:username' do
  erb :hello, locals: { username: params[:username] }
end

get '/hello/:first_name/:last_name' do
  "Hello, #{params[:first_name]} === #{params[:last_name]}"
end

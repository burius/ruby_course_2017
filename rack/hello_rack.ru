class HelloRack

  def call(env)
    if env['REQUEST_METHOD'] == 'GET' && env['PATH_INFO'] == '/hello'
      [
        200,
        {'Content-Type' => 'text/html'},
        ['Hello, world!']
      ]
    end
  end

end

run HelloRack.new

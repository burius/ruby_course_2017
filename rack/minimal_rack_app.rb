require 'rack'

min_app = proc do |env|
  [
    200,
    {'Content-Type' => 'application/json'},
    ['Minimal Rack App']
  ]
end

Rack::Handler.default.run min_app

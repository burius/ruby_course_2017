require 'sinatra'

class HelloSinatra < Sinatra::Base

  get '/hello' do
    'Hello, world!'
  end

end

run HelloSinatra

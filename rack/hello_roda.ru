require 'roda'

class HelloRoda < Roda

  route do |r|
    r.on 'hello' do
      r.get 'world' do
        'Hello, world!'
      end
      r.get 'students' do
        'Hello, students!'
      end
    end
    #r.get 'hello' do
    #  'Hello, world!'
    #end
  end

end

run HelloRoda.freeze.app

Then(/^there should be a blog post with title "([^"]*)"$/) do |title|
  expect(::BlogPost.find_by(title: title)).to be
end

And(/^there exists blog post with title "([\w\s]*)" and text "([^"]*)"$/) do |title, text|
  ::BlogPost.create title: title, text: text, user: @user
end

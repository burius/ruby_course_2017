Given /^user exists$/ do
  @user = ::FactoryGirl.create :user, name: 'Test User'
end

Given /^admin user exists$/ do
  @user = ::FactoryGirl.create :admin_user, name: 'Test Admin'
end

Given /^user is logged in$/ do
  login_as(@user)
end

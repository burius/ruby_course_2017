Feature: Main flow

  Background:
    Given user exists
    Given I am on login page
    When I fill in "user[email]" field with "testman@bla.com"
    And I fill in "user[password]" field with "password"
    And I wait for 2 seconds
    And I click on "Log in"

  @javascript
  Scenario: User logs into the system and adds new blog post
    And I click on "Add new"
    And I wait for 2 seconds
    When I fill in "blog_post[title]" field with "Title goes here"
    When I fill in "blog_post[text]" field with "Text goes here bla bla bla"
    And I click on "Create"
    Then there should be a blog post with title "Title goes here"

  @javascript
  Scenario: User should be able to edit his blog post
    Given there exists blog post with title "Title" and text "Bla"
    And I go to blog_posts
    When I click on "Edit"
    And I wait for 2 seconds
    And I fill in "blog_post[title]" field with "Changed title"
    And I click on "Update"
    And I wait for 2 seconds
    Then there should be a blog post with title "Changed title"

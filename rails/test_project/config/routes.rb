Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'blog_posts#index'
  match '/hello/:id', to: 'home#hello', via: :get

  resources :blog_posts
end

class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name, limit: 250, null: false, default: ''
      t.string :email, limit: 100, null: false, default: ''
      t.timestamps
    end
  end
end

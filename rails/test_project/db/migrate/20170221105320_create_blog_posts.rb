class CreateBlogPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :blog_posts do |t|
      t.string :title, limit: 200, null: false, default: ''
      t.text :text
      t.datetime :date
      t.integer :user_id
      t.timestamps
    end
  end
end

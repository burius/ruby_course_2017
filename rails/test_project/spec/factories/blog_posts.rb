FactoryGirl.define do
  factory :blog_post do
    title 'Test title'
    text 'Foo baz bar'
  end
end

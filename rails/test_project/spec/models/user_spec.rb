require 'rails_helper'

describe ::User, type: :model do

  subject { ::FactoryGirl.create :user }

  context '#blog_posts_count' do
    
    it 'should return 0 if user has no blog posts' do
      expect(subject.blog_posts_count).to equal(0)
    end

    it 'should return blog posts number if user has one blog post' do
      ::FactoryGirl.create :blog_post, user: subject
      expect(subject.blog_posts_count).to equal(1)
    end

    it 'should return blog posts number if user has multiple blog posts' do
      ::FactoryGirl.create :blog_post, user: subject
      ::FactoryGirl.create :blog_post, user: subject
      ::FactoryGirl.create :blog_post, user: subject
      expect(subject.blog_posts_count).to equal(3)
    end
  end
end

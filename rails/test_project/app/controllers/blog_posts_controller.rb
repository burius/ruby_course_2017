class BlogPostsController < ApplicationController
  before_action :authenticate_user!, except: :index
  caches_page :index

  def index
    @blog_posts = ::BlogPost.joins(:user)
                            .accessible_by(current_ability)
                            .select('blog_posts.*', 'users.name AS user_name')
  end

  def new
    @blog_post = ::BlogPost.new
  end

  def create
    @blog_post = ::BlogPost.new
    @blog_post.attributes = blog_post_params
    @blog_post.user_id = current_user.id
    success = @blog_post.save
    redirect_to blog_posts_path
  end

  def edit
    @blog_post = ::BlogPost.find params[:id]
  end

  def update
    @blog_post = ::BlogPost.find params[:id]
    @success = @blog_post.update_attributes(blog_post_params)
    respond_to do |format|
      format.html do
        redirect_to blog_posts_path
      end
      format.js do
        if @success
          @blog_posts = ::BlogPost.all
        else
          @blog_post.errors.add('title', 'Another error')
          logger.info "Blog post errors: #{@blog_post.errors.full_messages}"
        end
      end
    end
  end

  def destroy
    @blog_post = ::BlogPost.find params[:id]
    authorize!(:delete, @blog_post)
    @blog_post.destroy
    redirect_to blog_posts_path
  end

  private

  def blog_post_params
    params.require(:blog_post).permit(:title, :text)
  end
end

class HomeController < ApplicationController
  def index
    ::Rails.logger.info 'Index method called'
    render json: {a: 1, b: 2}
  end

  def hello
    @user = ::User.find params[:id]
  end
end

class A
  ['foo', 'baz', 'bar'].each do |el|
    define_method "get_#{el}".to_sym do
      puts "getting #{el}"
    end
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^foo_[\d]+$/
      "This method exists: #{method_name}"
    else
      super(method_name, *args, &block)
    end
  end
end

class Object
  def self.static_method
    'static method'
  end
  class << self
    def another_static_method
      'another static method'
    end
  end

  def metaclass
    class << self
      self
    end
  end
end

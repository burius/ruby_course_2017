module MyNamespace
end

module MyNamespace
  class A
    @@class_var = 0

    def self.static_method
      'Class method he-he'
    end

    def inc
      @@class_var += 1
    end

    def initialize(b = 'default value')
      @b = b
      puts "Initialized class A with param #{b}"
    end

    def bla
      @a.to_s + ' foo baz bar ' + private_method
    end

    def foo(param)
      a = param
      @a = if param.is_a?(::Fixnum)
             param * 2
           else
             param
           end
      @a = param.is_a?(::Fixnum) ? param * 2 : param
      puts 'Hello!'
    end

    private

    def private_method
      'private...'
    end
  end
end

class MyNamespace::B < MyNamespace::A
  def bla
    "#{super()} he-he"
  end

  def foo(param)
    "#{param} -- #{empty}"
  end

  def empty
  end
end

module M
  def self.module_static_method
    'defined in module!!'
  end

  def module_method
    'defined in the module!'
  end
end

class C < MyNamespace::B
  include M
  def empty
    'not that empty!'
  end
end

class AnotherClass
  include M
end

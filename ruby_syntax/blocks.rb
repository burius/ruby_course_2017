a = 1
b = case a
    when 1 then 'one'
    when 2 then 'two'
    when 3 then 'four'
    else 'many'
    end

class A
  attr_accessor :a

  def initialize(a)
    @a = a
  end

  def ===(other_value)
    @a == other_value.a
  end

  def method_with_block(b)
    @b = b
    yield(b) if block_given?
    puts 'method ends here'
  end
end

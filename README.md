# Ruby and Rails Programming Course, NaUKMA 2017

### Оголошення
* Хто досі не здав проект - приносьте на залік... Не залишайте роботу на останній вечір: встигнути буде нереально!
* Залік починається 25.04.2017 об 11:40 в 1-310.
* Наступна лекція (18.04.2017) відбудеться (хоча в розкладі її уже немає). Будемо говорити про performance, scalability та інші цікаві речі.
* **Розгляд/захист проектів** => 18.04.2017 13:30 - 17:50
* FYI, проект на не-Rails Rack фреймворці (Sinatra, Roda, Cuba, Padrino, Hanami, etc) = +5 балів.

### Матеріали Курсу
* [Lecture screencasts (FTP)](ftp://94.45.82.58/2017) login/password: student/Students
* [Робочий тематичний план (GoogleDocs)](https://docs.google.com/document/d/11yrjjiSQ49Fb6wZ7ytgMdkGpWUKV1sFcPGCSSjZk2WA/edit?usp=sharing)
* [Repository попереднього року (BitBucket)](https://bitbucket.org/burius/ror_course)

### Контрольний Проект
* [Вимоги до проекта](https://bitbucket.org/burius/ruby_course_2017/src/81c940b75b67ba8349a8e3dcbf7b33a58532c83c/students/?at=master) - див. README

### Useful Links
#### Ruby
* [Ruby style guide](https://github.com/bbatsov/ruby-style-guide) by bbatsov
* [Official Ruby documentation (core API)](http://ruby-doc.org/core-2.3.0/)
#### Rails
* [Rails API documentation](http://api.rubyonrails.org/)
* [Official Rails guides](http://guides.rubyonrails.org/)
* [JQuery-ujs Wiki](https://github.com/rails/jquery-ujs/wiki)
#### Client-side
* [Bootstrap CSS/Javascript framework](http://getbootstrap.com/)
* [CoffeScript](http://coffeescript.org/)

### Legendary Rubyists on the Web
* [matz](https://twitter.com/yukihiro_matz) - Yukihiro Matsumoto, author of Ruby. MINASWAN (Matz is Nice And So We Are Nice)
* [dhh](https://twitter.com/dhh) - David Heinemeier Hansson, initial author of Ruby on Rails
* [tenderlove](https://twitter.com/tenderlove) - Aaron Patterson, Rails core contributor and a very nice guy
* [wycatz](https://twitter.com/wycats) - Yehuda Katz, initial author of Merb; ruby, javascript and rust hacker
* [josevalim](https://twitter.com/josevalim) - José Valim, one of the top Rails contributors, author of Elixir language
